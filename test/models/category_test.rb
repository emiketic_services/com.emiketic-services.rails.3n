# == Schema Information
#
# Table name: categories
#
#  id                         :integer          not null, primary key
#  name                       :string
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  slug                       :string
#  parent_id                  :integer
#  lft                        :integer
#  rgt                        :integer
#  depth                      :integer
#  children_count             :integer
#  icon_file_name             :string
#  icon_content_type          :string
#  icon_file_size             :integer
#  icon_updated_at            :datetime
#  description                :text
#  icon_home_file_name        :string
#  icon_home_content_type     :string
#  icon_home_file_size        :integer
#  icon_home_updated_at       :datetime
#  rank                       :integer          default(0)
#  background_file_name       :string
#  background_content_type    :string
#  background_file_size       :integer
#  background_updated_at      :datetime
#  active                     :boolean          default(TRUE), not null
#  delete_icon                :boolean          default(FALSE), not null
#  delete_home_icon           :boolean          default(FALSE), not null
#  delete_background          :boolean          default(FALSE), not null
#  acceuil_photo_file_name    :string
#  acceuil_photo_content_type :string
#  acceuil_photo_file_size    :integer
#  acceuil_photo_updated_at   :datetime
#

require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
