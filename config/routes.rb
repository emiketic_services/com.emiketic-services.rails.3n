Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  get 'company', to: 'static_pages#company', as: 'company'
  get 'gterms', to: 'static_pages#gterms', as: 'gterms'
  get 'service', to: 'static_pages#service', as: 'service'
  get 'mention', to: 'static_pages#mention', as: 'mention'
  get 'contact', to: 'contacts#index', as: 'contact'
  get 'team_members', to: 'team_members#_team_members', as: 'team_members'
  resources :contacts, only: [:new, :create]

  resources :categories, only: [:show] do
    resources :products, only: [:show]
  end

  resources :promotions, only: [:index]
  resources :comments

  root 'static_pages#home'
end
