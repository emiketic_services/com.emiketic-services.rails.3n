# config valid only for current version of Capistrano
lock '3.4.1'

set :application, '3n'
set :repo_url, 'git@bitbucket.org:emiketic_services/com.emiketic-services.rails.3n.git'
set :branch, 'master'

set :deploy_to, '/home/deploy/3n'

set :linked_files, %w{config/database.yml config/secrets.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

set :bundle_binstubs, nil

namespace :deploy do

  desc 'Restart application'
  task :restart do
    invoke 'unicorn:reload'
    # on roles(:app), in: :sequence, wait: 5 do
    #   execute :touch, release_path.join('tmp/restart.txt')
    # end
  end

  after :publishing, :restart
  after :finishing,  :cleanup
end
