# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170307121132) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "albums", force: :cascade do |t|
    t.string   "name"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "albums", ["product_id"], name: "index_albums_on_product_id"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "slug"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.integer  "children_count"
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.text     "description"
    t.string   "icon_home_file_name"
    t.string   "icon_home_content_type"
    t.integer  "icon_home_file_size"
    t.datetime "icon_home_updated_at"
    t.integer  "rank",                       default: 0
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.boolean  "active",                     default: true,  null: false
    t.boolean  "delete_icon",                default: false, null: false
    t.boolean  "delete_home_icon",           default: false, null: false
    t.boolean  "delete_background",          default: false, null: false
    t.string   "acceuil_photo_file_name"
    t.string   "acceuil_photo_content_type"
    t.integer  "acceuil_photo_file_size"
    t.datetime "acceuil_photo_updated_at"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

# Could not dump table "comments" because of following NoMethodError
#   undefined method `[]' for nil:NilClass

  create_table "company_details", force: :cascade do |t|
    t.string   "phones"
    t.string   "emails"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "email"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "product_id"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "phone"
    t.string   "fax"
    t.string   "company"
    t.string   "address"
  end

  create_table "details", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "address"
  end

  create_table "emails", force: :cascade do |t|
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "detail_id"
  end

  add_index "emails", ["detail_id"], name: "index_emails_on_detail_id"

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "event_image_file_name"
    t.string   "event_image_content_type"
    t.integer  "event_image_file_size"
    t.datetime "event_image_updated_at"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "document_pdf_file_name"
    t.string   "document_pdf_content_type"
    t.integer  "document_pdf_file_size"
    t.datetime "document_pdf_updated_at"
  end

  create_table "featured_products", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.integer  "product_id"
  end

  add_index "featured_products", ["product_id"], name: "index_featured_products_on_product_id"

  create_table "images", force: :cascade do |t|
    t.integer  "album_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "content_file_name"
    t.string   "content_content_type"
    t.integer  "content_file_size"
    t.datetime "content_updated_at"
    t.integer  "product_id"
  end

  add_index "images", ["album_id"], name: "index_images_on_album_id"

  create_table "partners", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "phones", force: :cascade do |t|
    t.string   "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "detail_id"
  end

  add_index "phones", ["detail_id"], name: "index_phones_on_detail_id"

  create_table "photos", force: :cascade do |t|
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "cont_file_name"
    t.string   "cont_content_type"
    t.integer  "cont_file_size"
    t.datetime "cont_updated_at"
    t.integer  "partner_id"
    t.string   "partner_url"
  end

  add_index "photos", ["partner_id"], name: "index_photos_on_partner_id"

  create_table "product_sliders", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "document_pdf_file_name"
    t.string   "document_pdf_content_type"
    t.integer  "document_pdf_file_size"
    t.datetime "document_pdf_updated_at"
    t.integer  "category_id"
    t.string   "slug"
    t.integer  "promotion_id"
    t.string   "url"
    t.boolean  "new",                       default: false, null: false
    t.string   "status"
    t.string   "state"
    t.integer  "user_id"
    t.string   "video_url"
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id"
  add_index "products", ["promotion_id"], name: "index_products_on_promotion_id"
  add_index "products", ["slug"], name: "index_products_on_slug", unique: true

  create_table "promotions", force: :cascade do |t|
    t.string   "name"
    t.integer  "percentage"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "flyer_file_name"
    t.string   "flyer_content_type"
    t.integer  "flyer_file_size"
    t.datetime "flyer_updated_at"
    t.text     "description"
    t.string   "promo_type"
  end

  create_table "sliders", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "activated",                 default: true, null: false
    t.text     "description"
    t.string   "slider_image_file_name"
    t.string   "slider_image_content_type"
    t.integer  "slider_image_file_size"
    t.datetime "slider_image_updated_at"
  end

  create_table "team_members", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.string   "linkedin"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.boolean  "active",               default: true, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
