class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.date :date_creat
      t.time :time_start
      t.time :time_end

      t.timestamps null: false
    end
  end
end
