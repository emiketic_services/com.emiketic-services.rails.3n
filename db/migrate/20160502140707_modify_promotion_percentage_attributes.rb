class ModifyPromotionPercentageAttributes < ActiveRecord::Migration
  def change
    change_column :promotions, :percentage, :integer
  end
end
