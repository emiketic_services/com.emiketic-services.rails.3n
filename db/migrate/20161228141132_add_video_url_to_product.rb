class AddVideoUrlToProduct < ActiveRecord::Migration
  def change
    add_column :products, :video_url, :string
  end
end
