class AddDetailsRefToEmail < ActiveRecord::Migration
  def change
    add_reference :emails, :detail, index: true, foreign_key: true
  end
end
