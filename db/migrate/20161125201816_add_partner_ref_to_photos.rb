class AddPartnerRefToPhotos < ActiveRecord::Migration
  def change
    add_reference :photos, :partner, index: true, foreign_key: true
  end
end
