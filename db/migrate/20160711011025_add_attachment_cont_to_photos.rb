class AddAttachmentContToPhotos < ActiveRecord::Migration
  def self.up
    change_table :photos do |t|
      t.attachment :cont
    end
  end

  def self.down
    remove_attachment :photos, :cont
  end
end
