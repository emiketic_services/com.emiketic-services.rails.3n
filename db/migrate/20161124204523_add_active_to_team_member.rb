class AddActiveToTeamMember < ActiveRecord::Migration
  def change
    add_column :team_members, :active, :boolean, null: false, default: true
  end
end
