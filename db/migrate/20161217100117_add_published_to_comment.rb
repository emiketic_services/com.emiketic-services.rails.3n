class AddPublishedToComment < ActiveRecord::Migration
  def change
    add_column :comments, :published, :boolean, null: false, default: true
  end
end
