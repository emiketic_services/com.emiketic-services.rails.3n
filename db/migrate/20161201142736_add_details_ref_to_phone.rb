class AddDetailsRefToPhone < ActiveRecord::Migration
  def change
    add_reference :phones, :detail, index: true, foreign_key: true
  end
end
