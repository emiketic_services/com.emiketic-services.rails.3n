class AddAttachmentFlyerToPromotions < ActiveRecord::Migration
  def self.up
    change_table :promotions do |t|
      t.attachment :flyer
    end
  end

  def self.down
    remove_attachment :promotions, :flyer
  end
end
