class AddPartnerUrlToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :partner_url, :string
  end
end
