class AddActivatedToSlider < ActiveRecord::Migration
  def change
    add_column :sliders, :activated, :boolean, null: false, default: true
  end
end
