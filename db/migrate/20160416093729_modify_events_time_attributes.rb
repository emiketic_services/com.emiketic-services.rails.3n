class ModifyEventsTimeAttributes < ActiveRecord::Migration
  def change
    remove_column :events, :date_creat
    remove_column :events, :time_start
    remove_column :events, :time_end

    add_column :events, :start_date, :datetime
    add_column :events, :end_date, :datetime
  end
end
