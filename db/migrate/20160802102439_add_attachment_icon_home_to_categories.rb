class AddAttachmentIconHomeToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :icon_home
    end
  end

  def self.down
    remove_attachment :categories, :icon_home
  end
end
