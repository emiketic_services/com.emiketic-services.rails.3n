class AddAttachmentAcceuilPhotoToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :acceuil_photo
    end
  end

  def self.down
    remove_attachment :categories, :acceuil_photo
  end
end
