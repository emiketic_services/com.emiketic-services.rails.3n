class AddProductRefToFeaturedProducts < ActiveRecord::Migration
  def change
    add_reference :featured_products, :product, index: true, foreign_key: true
  end
end
