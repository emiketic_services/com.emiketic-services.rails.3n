class AddPromotionReferencesToProduct < ActiveRecord::Migration
  def change
    add_reference :products, :promotion, index: true, foreign_key: true
  end
end
