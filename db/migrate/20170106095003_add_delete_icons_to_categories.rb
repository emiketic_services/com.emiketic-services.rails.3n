class AddDeleteIconsToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :delete_icon, :boolean, null: false, default: false
    add_column :categories, :delete_home_icon, :boolean, null: false, default: false
  end
end
