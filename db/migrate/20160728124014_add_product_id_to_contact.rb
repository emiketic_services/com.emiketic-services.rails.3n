class AddProductIdToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :product_id, :integer
  end
end
