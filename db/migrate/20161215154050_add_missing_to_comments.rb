class AddMissingToComments < ActiveRecord::Migration
  def change
    add_column :comments, :user, :string
    add_column :comments, :usermail, :string
    add_column :comments, :reponseadmin, :text
    add_column :comments, :active, :boolean, null: false, default: false
  end
end
