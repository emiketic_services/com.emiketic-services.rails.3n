class AddAttachmentSliderImageToSliders < ActiveRecord::Migration
  def self.up
    change_table :sliders do |t|
      t.attachment :slider_image
    end
  end

  def self.down
    remove_attachment :sliders, :slider_image
  end
end
