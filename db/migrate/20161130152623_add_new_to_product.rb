class AddNewToProduct < ActiveRecord::Migration
  def change
    add_column :products, :new, :boolean, null: false, default: false
  end
end
