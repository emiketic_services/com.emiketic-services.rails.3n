class AddAttachmentCoverToFeaturedProducts < ActiveRecord::Migration
  def self.up
    change_table :featured_products do |t|
      t.attachment :cover
    end
  end

  def self.down
    remove_attachment :featured_products, :cover
  end
end
