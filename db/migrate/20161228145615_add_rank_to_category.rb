class AddRankToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :rank, :integer, default: 0
  end
end
