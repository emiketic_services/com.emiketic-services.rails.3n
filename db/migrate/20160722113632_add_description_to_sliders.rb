class AddDescriptionToSliders < ActiveRecord::Migration
  def change
    add_column :sliders, :description, :text
  end
end
