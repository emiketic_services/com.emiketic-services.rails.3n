class AddDeleteBackgroundToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :delete_background, :boolean, null: false, default: false
  end
end
