class RemoveReferenceFromContact < ActiveRecord::Migration
  def change
    remove_column :contacts, :reference, :string
  end
end
