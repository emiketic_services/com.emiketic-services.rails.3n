class AddPromoTypeToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :promo_type, :string
  end
end
