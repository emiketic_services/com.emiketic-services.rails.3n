class AddAttachmentDocumentPdfToEvents < ActiveRecord::Migration
  def self.up
    change_table :events do |t|
      t.attachment :document_pdf
    end
  end

  def self.down
    remove_attachment :events, :document_pdf
  end
end
