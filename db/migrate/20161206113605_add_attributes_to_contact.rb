class AddAttributesToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :phone, :string
    add_column :contacts, :fax, :string
    add_column :contacts, :company, :string
    add_column :contacts, :address, :string
  end
end
