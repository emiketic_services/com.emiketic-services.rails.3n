# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
unless AdminUser.find_by_email('admin@example.com')
  AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
end

unless Partner.exists?
  # create Partner default images
  params = {
    partner: {
      photos_attributes: [
        {
          cont: File.new("#{Rails.root}/app/assets/images/slide-01.jpg")
        }
      ]
    }
  }
  Partner.create(params[:partner])
end
unless Detail.exists?
  params = {
    detail: {
      phones_attributes: [
        number: "123456789"
      ]
    }
  }
  Detail.create(params[:detail])
end
if Rails.env.development?
  5.times do
    # Create events

    promotion = Promotion.create(
      name: Faker::Lorem.words.join(' '),
      description: Faker::Lorem.paragraph,
      percentage: Faker::Number,
      flyer: File.new("#{Rails.root}/app/assets/images/events.jpg")
    )
  end

  # Create categories
  Category::PARENT_CATEGORIES.each do |name|
    category = Category.create(name: name, description: Faker::Lorem.paragraph)
    3.times do
      category.children.create(name: Faker::Lorem.words.join(' '), description: Faker::Lorem.paragraph)
    end
  end

  # Create products
  Category.all.each do |category|
    5.times do
      product = category.products.build(
        name: Faker::Lorem.words.join(' '),
        description: Faker::Lorem.paragraph,
        price: Faker::Number.decimal(3),
        images_attributes: [
          {
            content: File.new("#{Rails.root}/app/assets/images/steadicam1.jpg")
          },
          {
            content: File.new("#{Rails.root}/app/assets/images/steadicam2.jpg")
          },
          {
            content: File.new("#{Rails.root}/app/assets/images/steadicam3.jpg")
          }
        ]
      )
      product.save

    end
  end
end
