var ready;
ready = function() {
  loadField();
  displayPercentageField();
  changeUpdateUrl();
};

$(document).ready(ready);
$(document).on('page:load', ready);


loadField = function(){
  if($('#product_state_en_promotion').attr("checked")) {
      $("#product_promotion_input").show();
  }
  else{
    $("#product_promotion_input").hide();
  }
}
displayPercentageField = function(){
  $('.status').click(function(){
    if( $(this).val() == ("En promotion")){
      $("#product_promotion_input").show();
    }
    else{
      $("#product_promotion_id").val($("#product_promotion_id option:first").val());
      $("#product_promotion_input").hide();
    }
  });
}
changeUpdateUrl = function() {
  var backUrl = $('.action_item a').attr('href');
  $('#product_submit_action').click(function(){
    window.location.assign(backUrl);
  });
};
