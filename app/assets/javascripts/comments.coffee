ready = ->
  $('#new_comment').bind 'ajax:success', (event, data, status, xhr) ->
      clear_form_errors()
      $('#alert_placeholder').hide().append('<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button> Votre Message a été envoyer et sera traiter </div>').fadeIn()
      $('#alert_placeholder').delay(2000).fadeOut()

  $('#new_comment').on("ajax:error", (e, data, status, xhr) ->
    render_form_errors('comment', data.responseJSON)
  )

$(document).ready(ready)
$(document).on('page:load', ready)

render_form_errors = (model_name, errors) ->
  errorMessage = '';
  $.each(errors, (field, messages) ->
    error = "<strong>" + fieldName(field)+"</strong>"+ " " + $.map(messages, (m) -> errorMessageText(m)) + "<br />";
    errorMessage += error;

  )
  if errorMessage != ''
    $('#alert_placeholder').hide().append('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>'+errorMessage+'</div>').fadeIn()
    $('.alert').delay(2000).fadeOut()

  #$('#alert_placeholder').empty()
fieldName = (field) ->
  switch field
    when "body" then return "Message:"
    when "usermail" then return "Email:"
    else return "Nom d'utilisateur:"

errorMessageText = (errorMessage) ->
  switch errorMessage
    when "can't be blank" then return "Ne doit pas être vide"
    else return "Veuillez inclure une adresse email valide "

clear_form_errors = ->
  this.find('input').val('')
