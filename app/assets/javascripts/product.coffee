ready = ->
	displayProduct()
	quantityComm()
	getVideosUrl()

$(document).ready(ready)
$(document).on('page:load', ready)

quantityComm = ->
	$('.spinner .btn:first-of-type').on 'click', ->
  	$('.spinner input').val parseInt($('.spinner input').val(), 10) + 1

	$('.spinner .btn:last-of-type').on 'click', ->
	  if parseInt($('.spinner input').val(), 10) - 1 >= 0
	    $('.spinner input').val parseInt($('.spinner input').val(), 10) - 1

displayProduct = ->
	$('.carousel-item').click (e) ->
		e.preventDefault()
		$('#image-preview').html('<img src="' + $(this).data("image") + '"/>')

getVideoId = (url) ->
	videoId = url.split("watch?v=")[1]
	return videoId

getVideosUrl = ->
	$('.video-url').each ->
		url = $(this).attr('href')
		videoId = getVideoId(url)
		srcUrl = "https://www.youtube.com/embed/" + videoId
		$(this).closest('.embed-responsive').find('iframe').attr("src", srcUrl)
