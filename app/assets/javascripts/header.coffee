ready = ->
  showMenu()
  showMobileMenu()

$(document).ready(ready)
$(document).on('page:load', ready)

showMenu = ->
  $('#product > a').click (e)->
    e.preventDefault()
    $(this).parent().parent().find(".active-nav").removeClass("active-nav")
    $(this).addClass("active-nav")
    if $('.divider').css('display') == 'block'
      $('.divider').css('display','none')
    else
      $('.divider').css('display','block')

    $('.menu').slideToggle("fast")

showMobileMenu = ->
  $("#product").on 'click', ->
    $(".nav-category").css('display','block')
  $('.category-title').on 'click', ->
    $(this).parent().children('.category-child').toggle()
