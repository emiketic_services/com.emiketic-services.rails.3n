ready =->

	showFullName()
	rangePrice()
	FilterPrice()
	FilterByNew()
	FilterByDisponibility()
	expandFilter()

$(document).ready(ready)
$(document).on('page:load', ready)

showFullName =->
	$('[data-toggle="tooltip"]').tooltip()
rangePrice =->
	$('.filter-rang').slider().on 'slide', ->
	#$('.filter-rang').slider  ->
		minVal = $('.min-slider-handle').attr('aria-valuenow')
		$(".min-price").html(minVal+"<sup>DT</sup>")
		maxVal = $('.max-slider-handle').attr('aria-valuenow')
		$(".max-price").html(maxVal+"<sup>DT</sup>")
  	#console.log $('#slider').data('slider').min
FilterPrice = ->
	$('.filter-rang').slider().on 'slideStop', (e) ->
		e.preventDefault()
		$('#q_price_gteq').val($('.min-slider-handle').attr('aria-valuenow'))
		$('#q_price_lteq').val($('.max-slider-handle').attr('aria-valuenow'))
		$('#product_search').submit()

expandFilter =->
	## Expand and change font-awesome icon
	$('.collapse').on 'show.bs.collapse', (e) ->
		clicked = $(document).find('[href=\'#' + $(e.target).attr('id') + '\']')
		$(clicked).find('i').addClass('fa-minus').removeClass('fa-plus');
		if $('.categ-filter-title').last().attr('aria-expanded')
			$('.categ-filter-title span').last().css({
				"border-bottom": "1px solid #e5e5e5"
			})

	$('.collapse').on 'hide.bs.collapse', (e) ->
		clicked = $(document).find('[href=\'#' + $(e.target).attr('id') + '\']')
		$(clicked).find('i').addClass('fa-plus').removeClass('fa-minus');
		if $('.categ-filter-title').last().attr('aria-expanded')
			$('.categ-filter-title span').last().css({
				"border-bottom": "0px solid #e5e5e5"
			})

FilterByNew = ->
	$('.filter-disp .new').on 'click', (e) ->
		input = $('<input>').attr('type', 'hidden').attr('name', 'isNew')
		if $(this).hasClass('fa-square-o')
			$(this).removeClass('fa-square-o').addClass('fa-check-square ')
			$('#product_search').append $(input.val(true))
		else
			$(this).addClass('fa-square-o').removeClass('fa-check-square ')
			$('#product_search').append $(input.val(false))
		$('#product_search').submit()

FilterByDisponibility = ->
	$('.filter-disp .available').on 'click', (e) ->
		input = $('<input>').attr('type', 'hidden').attr('name', 'available')
		if $(this).hasClass('fa-square-o')
			$(this).removeClass('fa-square-o').addClass('fa-check-square ')
			$('#product_search').append $(input.val(true))
		else
			$(this).addClass('fa-square-o').removeClass('fa-check-square ')
			$('#product_search').append $(input.val(false))
		$('#product_search').submit()
