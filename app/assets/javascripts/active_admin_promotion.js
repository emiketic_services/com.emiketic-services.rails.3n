var ready;
ready = function() {
  loadField();
  displayPercentageField();
};

$(document).ready(ready);
$(document).on('page:load', ready);


loadField = function(){
  if($('#promotion_promo_type_en_promotion').attr("checked")) {
      $("#promotion_percentage_input").show();
  }
  else{
    $("#promotion_percentage_input").hide();
  }
}
displayPercentageField = function(){
  $('.promo').click(function(){
    if( $(this).val() == ("En promotion")){
      $("#promotion_percentage_input").show();
    }
    else{
      $("#promotion_percentage_input").hide();
    }
  });
}
