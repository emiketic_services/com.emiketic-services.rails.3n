class ContactsController < ApplicationController

  def index
    @products = Product.all
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      ContactMailer.forward_email(@contact).deliver_later
      # render js: "window.location='#{root_path}'"
      render json: {}, status: 200
    else
      render json: @contact.errors.messages, status: 422
    end

  end



  private
    def contact_params
      params.require(:contact).permit(:email, :product_id, :message, :firstname, :lastname, :phone, :fax, :company, :address)
    end
end
