class CategoriesController < ApplicationController
  def show
    @category = Category.friendly.find(params[:id])
    if params[:isNew] == "true"
      @q = @category.related_products.is_new
      @products = @q
    elsif
        params[:available] == "true"
        @q = @category.related_products.is_available
        @products = @q
      else
        @q = @category.related_products.ransack(params[:q])
        @products = @q.result(distinct: true)
    end
  end
end
