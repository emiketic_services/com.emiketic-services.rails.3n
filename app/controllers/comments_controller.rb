class CommentsController < ApplicationController

  def create
   commentable = commentable_type.constantize.find(commentable_id)
   @comment = Comment.build_from(commentable, body, user, usermail)


   if @comment.save
     make_child_comment
     render js: "window.location= window.location.pathname"
   else
     render json: @comment.errors.messages, status: 422
   end

  end

  private


    def comment_params
     params.require(:comment).permit(:body, :commentable_id, :commentable_type, :comment_id, :user, :usermail)
    end

    def commentable_type
     comment_params[:commentable_type]
    end

    def commentable_id
     comment_params[:commentable_id]
    end

    def comment_id
     comment_params[:comment_id]
    end

    def body
     comment_params[:body]
    end

    def user
     comment_params[:user]
    end

    def usermail
     comment_params[:usermail]
    end

    def make_child_comment
     return "" if comment_id.blank?

     parent_comment = Comment.find comment_id
     @comment.move_to_child_of(parent_comment)
    end

end
