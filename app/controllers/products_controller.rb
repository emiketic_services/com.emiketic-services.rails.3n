class ProductsController < ApplicationController
  def show
    @category = Category.friendly.find(params[:category_id])
    @product = Product.friendly.find(params[:id])
    @new_comment = Comment.build_from(@product, "", "", "")
  end
end
