class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  skip_before_action :verify_authenticity_token
  before_action :set_category, :set_slider, :set_partners, :_team_members,
                :set_featured_products, :get_company_details

  def set_category
    @root_categories = Category.roots
  end

  def set_slider
    @activated = Slider.all
  end

  def set_featured_products
    @featured_products = FeaturedProduct.all
  end

  def set_partners
    @partners = Partner.all
  end

  def get_company_details
    @details = Detail.all
  end

  def _team_members
    @team_members = TeamMember.all
  end

end
