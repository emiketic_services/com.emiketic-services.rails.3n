class ContactMailer < ApplicationMailer
  ActionMailer::Base.smtp_settings = {
    address:              "smtp.gmail.com",
    port:                 587,
    user_name:            ENV['SMTP_EMAIL'],
    password:             ENV['SMTP_PASSWORD'],
    authentication:       'plain',
    enable_starttls_auto: true
  }
  default from: ENV['SMTP_EMAIL']

  def forward_email(contact)
    @contact = contact
    mail(to: ENV['SMTP_EMAIL_DEST'], subject: "Nouveau contact (#{@contact.email})")
  end
end
