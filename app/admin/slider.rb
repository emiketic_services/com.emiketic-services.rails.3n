ActiveAdmin.register Slider do

  before_filter :skip_sidebar!, :only => :index

  permit_params :name, :activated, :description, :slider_image

  index do

    column :slider_image do |image|
      image_tag image.slider_image.url(:thumb)
    end
    column :name
    column :activated
    column :description


    actions
  end

  show do
    attributes_table do
      row :slider_image do |image|
        image_tag image.slider_image.url(:medium)
      end
      row :name
      row :activated
      row :description
    end
  end

  form do |f|
    inputs 'General' do
      input :name
      input :activated, as: :boolean
      input :description
      inputs 'Attachment' do
        input :slider_image, as: :file, hint: image_tag(object.slider_image.url(:medium))
      end
    end

    f.actions
  end


end
