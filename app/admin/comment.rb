ActiveAdmin.register Comment , as: "ProductComment" do
  # Customize filter fields
  filter :commentable, collection: proc { Product.pluck(:name, :id)}
  filter :user
  filter :usermail
  filter :active, as: :check_boxes
  filter :created_at
  filter :updated_at

  permit_params :published, :active, :body, :user, :usermail, :reponseadmin, commentable_attributes: [:id, :commentable_type, :_destroy]

  action_item only: :index do
    if Comment.pluck(:published).all? {|comment| comment == true}
      link_to "Unpublish Comments", publish_admin_product_comments_path
    else
      link_to "Publish Comments", publish_admin_product_comments_path
    end
  end

  collection_action :publish do
    if Comment.pluck(:published).all? {|comment| comment == true}
      comments = Comment.update_all("published = 'false'")
    else
      comments = Comment.update_all("published = 'true'")
    end
    redirect_to action: :index
  end

  index do
    selectable_column
      column "Show Comment", :active
      column "Produit", :commentable
      column "Message", :body
      column "Client Name", :user
      column "Client Email", :usermail
      column "3N-response", :reponseadmin
      column :created_at

    actions
  end
  show do |f|
    attributes_table do
      row("Show Comment"){f.active}
      row("Produit") {f.commentable}
      row("Message"){f.body}
      row("Client Name"){f.user}
      row("Client Email"){f.usermail}
      row("3N-response"){f.reponseadmin}
      row :created_at
    end
  end
  form do |f|
    inputs 'Comments' do
      input :active
      input :user, label: "Client Name"
      input :usermail, label: "Client Email"
      input :body, label: "Message"
      input :reponseadmin, label: "3N-response"
    end
    f.actions
  end
end
