ActiveAdmin.register FeaturedProduct do

  permit_params :title, :description, :cover, :product_id

  index do

    column :cover do |image|
      image_tag image.cover.url(:thumb)
    end
    column :title
    column :description
    actions
  end

  show do
    attributes_table do
      row :cover do |image|
        image_tag image.cover.url(:thumb)
      end
      row :title
      row :description
    end
  end

  form do |f|
    inputs 'General' do
      input :title
      input :description
      input :product_id, as: :select, collection: Product.all.map{|u| ["#{u.name}", u.id]}
      inputs 'Attachment' do
        input :cover, as: :file, hint: image_tag(object.cover.url(:medium))
      end
    end
    f.actions
  end


end
