ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Contacts récentes" do
          ul do
            Contact.last(10).map do |contact|
              li  link_to(contact.email, admin_message_path(contact))
            end
          end
        end
      end
    end
  end
end
