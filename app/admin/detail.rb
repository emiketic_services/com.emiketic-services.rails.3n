ActiveAdmin.register Detail do

  actions :all, except: [:new, :destroy]

  permit_params :address, phones_attributes: [:id, :number, :_destroy],
    emails_attributes: [:id, :address, :_destroy]

  controller do
    def index
      redirect_to edit_resource_url(Detail.first)
    end
  end

  form do |f|
    inputs "Address" do
      f.input :address
    end
    inputs 'Phones' do
      f.has_many :phones do |phone|
        phone.input :number
        if (phone.object.number.present?)
          phone.input :_destroy, :as=> :boolean, :label => 'Remove phone number'
        end
      end
    end

    inputs 'Emails' do
      f.has_many :emails do |email|
        email.input :address
        if (email.object.address.present?)
          email.input :_destroy, :as=> :boolean, :label => 'Remove email address'
        end
      end
    end

    f.actions
  end

end
