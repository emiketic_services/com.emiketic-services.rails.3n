ActiveAdmin.register Promotion, as: "Promotion" do

  permit_params :name, :percentage, :description, :flyer, product_ids: []
  index do
    selectable_column

    column "Flyer" do |promotion|
      image_tag(promotion.flyer.url(:thumb))
    end

    column :products do |promotion|
      render partial: 'products', locals: {promotion: promotion}
    end

    column :name
    column :description
    column :percentage


    actions
  end

  show do
    attributes_table do
      row "Flyer" do |promotion|
        link_to(image_tag(promotion.flyer.url(:thumb)))
      end
      panel 'Products' do
        table_for promotion.products do
          column :name do |product|
            link_to product.name, admin_produit_path(product)
          end
        end
      end
      row :name
      row :description
      row :percentage
    end
  end

  form do |f|
    inputs 'General' do
      input :name
      input :description, as: :ckeditor
      input :percentage
      input :flyer, as: :file, hint: image_tag(f.object.flyer.url(:medium))
      input :products, as: :check_boxes
    end

    f.actions
  end

end
