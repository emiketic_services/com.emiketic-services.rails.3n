ActiveAdmin.register Partner do
  actions :all, except: [:new, :destroy]

  permit_params photos_attributes: [:id, :cont, :partner_id, :partner_url, :_destroy]

  form do |f|
    f.has_many :photos do |photo|
      photo.input :cont, as: :file, hint: image_tag(photo.object.cont.url(:medium)), :label => 'Photo'
      photo.input :partner_url
      if (photo.object.cont.present?)
        photo.input :_destroy, :as=> :boolean, :label => 'Remove Photo'
      end
    end
    f.actions
  end

  controller do
    def index
      redirect_to edit_resource_url(Partner.first)
    end
  end

end
