ActiveAdmin.register Category, as: "Categories" do

  # Customize filter fields
  filter :parent_id, as: :select, collection: proc{ Category.roots }
  filter :name_equals, as: :select, collection: proc{ Category.sub_category }
  filter :description
  filter :created_at
  filter :updated_at

  # mention the permitted params for this model
  permit_params :name, :parent_id, :delete_icon, :delete_home_icon,
    :rank, :active, :icon, :background, :delete_background,
    :description, :icon_home, :acceuil_photo

  index do
    selectable_column

    column :name
    column :parent_id
    column :description

    actions
  end

  show do |f|
    attributes_table do
      row :name
      if f.parent_id != nil
        row :parent_id
      else
        row "Image" do |categ|
          link_to(image_tag(categ.icon.url(:thumb)))
        end
        row "Image" do |categ|
          link_to(image_tag(categ.icon_home.url(:thumb)))
        end
      end
      row :description
    end
  end

  form do |f|
    inputs 'General' do
      if f.object.parent_id == nil
        input :rank, :label => 'Ordre d affichage des categories'
      end
      input :name
      if f.object.parent_id == nil
        input :acceuil_photo, as: :file, hint: image_tag(f.object.acceuil_photo.url(:medium))
        input :background, as: :file, hint: image_tag(f.object.background.url(:medium))
        if (f.object.background.present?)
          f.input :delete_background, :as=> :boolean, :required => false, :label => 'Remove Background'
        end
        input :active, label: 'Utiliser dans page d accueil '
        input :icon, as: :file, hint: image_tag(f.object.icon.url(:medium))
        if (f.object.icon.present?)
          f.input :delete_icon, :as=> :boolean, :required => false, :label => 'Remove icon'
        end
        input :icon_home, as: :file, hint: image_tag(f.object.icon_home.url(:medium))
        if (f.object.icon_home.present?)
          f.input :delete_home_icon, :as=> :boolean, :required => false, :label => 'Remove home icon'
        end
      end
      input :parent_id, as: :select, collection: Category.roots
      input :description, as: :ckeditor
    end
    f.actions
  end



end
