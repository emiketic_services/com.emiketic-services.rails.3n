ActiveAdmin.register Product, as: "Produits" do
  permit_params :name, :description, :price, :document_pdf,
                :category_id, :url, :promotion_id, :new, :state, :video_url,
                images_attributes: [:id, :content, :_destroy]
  action_item except: [:index] do
    if !request.referrer.include? "edit"
      session[:return_to] ||= request.referrer
      link_to "Back", session.delete(:return_to)
    end
  end

  index do
    selectable_column
    column :new
    column :images do |produit|
      render partial: 'images', locals: {produit: produit}
    end
    column :name
    column :description
    column :price
    column :category
    column :video_url
    column :document_pdf do |product|
      product.document_pdf? ? link_to(product.document_pdf_file_name, product.document_pdf.url, target: '_blank') : ''
    end
    actions
  end

  show do |product|
    attributes_table do
      row :new
      row :name
      row :description
      row :price
      row :category
      row :document_pdf do
        product.document_pdf? ? link_to(product.document_pdf_file_name, product.document_pdf.url, target: '_blank') : ''
      end
      row :video_url
      row :state
      row :promotion
    end
    panel 'Images' do
      render partial: 'images', locals: {produit: resource}
    end
  end

  form remote:true do |f|

    inputs 'General' do
      input :new
      input :name
      input :description
      input :price
      input :document_pdf, hint: document_hint(f.object)
      input :category, as: :select,
            collection: nested_set_options(Category) {|i| "#{'-' * i.level} #{i.name}" }

      input :video_url
      input :state, as: :radio, collection: Product::PRODUCT_STATUS,
            input_html: { class: "status" }, :label => 'Status'
      input :promotion, as: :select, collection: Promotion.all.map{|u| ["#{u.name}", u.id]}
    end
    f.has_many :images do |image|
      image.input :content, as: :file, hint: image_tag(image.object.content.url(:thumb))
      if (image.object.content.present?)
        image.input :_destroy, :as=> :boolean, :label => 'Remove image'
      end
    end

    actions
  end

  controller do
    def destroy
      destroy! do |format|
        format.html { redirect_to :back }
      end
    end
  end
end
