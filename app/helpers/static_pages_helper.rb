module StaticPagesHelper
  def is_active(index)
    index == 0 ? 'active' : ''
  end

  def are_multiple(params)
    params.count() > 1 ? 'multiple' : 'only-one'
  end

  def active_page(page_path)
    current_page?(page_path) ? 'active-nav' : ''
  end

  def is_background(category)
    (category.acceuil_photo.present? && category.active) ? category.acceuil_photo : category.background
  end
  def static_map_for(lat, lng, options = {})
    params = {
      :center => [lat, lng].join(","),
      :zoom => 17,
      :size => "400x300",
      :markers => [lat, lng].join(","),
      :sensor => true
      }.merge(options)

    query_string =  params.map{|k,v| "#{k}=#{v}"}.join("&")
    image_tag "http://maps.googleapis.com/maps/api/staticmap?#{query_string}", alt: '3n-map', class: "threen-map"
  end


end
