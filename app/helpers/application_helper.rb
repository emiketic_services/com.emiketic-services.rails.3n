module ApplicationHelper
  def text_truncate (str, length)
    return str.length > length ? (str.slice(0, length) + '...').titleize : str.titleize;
  end

  def parag_truncate(str, length)
    return str.length > length ? str.slice(0, length) + '...' : str;
  end
end
