module SlidersHelper
  def is_active(index)
    index == 0 ? 'active' : ''
  end
end
