module ProductsHelper
  def document_hint(obj)
    if obj.document_pdf?
      content_tag :span do
        concat('PDF actuel:')
        concat(link_to(obj.document_pdf_file_name, obj.document_pdf.url, target: '_blank'))
      end
    end
  end
end
