# == Schema Information
#
# Table name: emails
#
#  id         :integer          not null, primary key
#  address    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  detail_id  :integer
#

class Email < ActiveRecord::Base
  belongs_to :detail
  validates_format_of :address,
    with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i,
    message: "Vous devez inserer un email"
end
