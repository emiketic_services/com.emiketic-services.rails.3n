# == Schema Information
#
# Table name: images
#
#  id                   :integer          not null, primary key
#  album_id             :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  content_file_name    :string
#  content_content_type :string
#  content_file_size    :integer
#  content_updated_at   :datetime
#  product_id           :integer
#

class Image < ActiveRecord::Base
  belongs_to :product

  has_attached_file :content, styles: { medium: "300x300>", thumb: "100x100>", xsmall: "150x150>", small: "200x200>", large: "350x350>", xlarge: "400x400>" },
                    default_url: "/images/:style/missing.png"
  validates_attachment_size :content, in: 0.megabytes..12.megabytes, message: "l'attachement doit être inférieur à 12mb"
  validates_attachment_content_type :content, content_type: /\Aimage\/.*\Z/
end
