# == Schema Information
#
# Table name: partners
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Partner < ActiveRecord::Base
  has_many :photos
  accepts_nested_attributes_for :photos, allow_destroy: true
end
