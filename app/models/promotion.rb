# == Schema Information
#
# Table name: promotions
#
#  id                 :integer          not null, primary key
#  name               :string
#  percentage         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  flyer_file_name    :string
#  flyer_content_type :string
#  flyer_file_size    :integer
#  flyer_updated_at   :datetime
#  description        :text
#  promo_type         :string
#

class Promotion < ActiveRecord::Base
  has_many  :products

  has_attached_file :flyer, :styles => { :medium => "300x300>", :thumb => "100x100#",small: "300x300>", large: "300x400>", cover: "400x500>",xxlarge: "500x600>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :flyer, :content_type => /\Aimage\/.*\Z/

  PROMOTION_TYPE = ["En promotion", "En stock", "Sur commande", "Import en cours"]


end
