# == Schema Information
#
# Table name: details
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  address    :string
#

class Detail < ActiveRecord::Base
  has_many :emails
  has_many :phones

  accepts_nested_attributes_for :emails, allow_destroy: true
  accepts_nested_attributes_for :phones, allow_destroy: true
end
