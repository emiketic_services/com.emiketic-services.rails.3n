# == Schema Information
#
# Table name: photos
#
#  id                :integer          not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  cont_file_name    :string
#  cont_content_type :string
#  cont_file_size    :integer
#  cont_updated_at   :datetime
#  partner_id        :integer
#  partner_url       :string
#

class Photo < ActiveRecord::Base
  belongs_to :partner

  has_attached_file :cont, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :cont, content_type: /\Aimage\/.*\Z/


end
