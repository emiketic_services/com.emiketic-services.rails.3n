# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  commentable_id   :integer
#  commentable_type :string
#  title            :string
#  body             :text
#  subject          :string
#  parent_id        :integer
#  lft              :integer
#  rgt              :integer
#  created_at       :datetime
#  updated_at       :datetime
#  username         :string
#  email            :string
#  response         :text
#  activate         :bool
#  name             :string
#  mail             :string
#  reponse          :text
#  show             :bool             default("f"), not null
#  user             :string
#  usermail         :string
#  reponseadmin     :text
#  active           :boolean          default(FALSE), not null
#  published        :boolean          default(TRUE), not null
#

class Comment < ActiveRecord::Base
  acts_as_nested_set scope: [:commentable_id, :commentable_type]

  validates :body, presence: true
  validates :user, presence: true
  validates :usermail, presence: true

  validates_format_of :usermail, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                      message: "Veuillez introduire une adresse email"

  belongs_to :commentable, polymorphic: true

  scope :find_comments_for_commentable, lambda { |commentable_str, commentable_id|
    where(:commentable_type => commentable_str.to_s, :commentable_id => commentable_id).order('created_at DESC')
  }

  scope :is_active, -> { where(active: true) }
  scope :is_published, -> {pluck(:published).all? {|comment| comment == true}}
  scope :related_products, -> {joins(:product)}

  def self.build_from(obj, comment, user, usermail)
    new \
      :commentable => obj,
      :body        => comment,
      :user        => user,
      :usermail    => usermail
  end

  def has_children?
    self.children.any?
  end

  def self.find_commentable(commentable_str, commentable_id)
    commentable_str.constantize.find(commentable_id)
  end

end
