# == Schema Information
#
# Table name: featured_products
#
#  id                 :integer          not null, primary key
#  title              :string
#  description        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  cover_file_name    :string
#  cover_content_type :string
#  cover_file_size    :integer
#  cover_updated_at   :datetime
#  product_id         :integer
#

class FeaturedProduct < ActiveRecord::Base

  belongs_to :product
  has_attached_file :cover, styles: { medium: "300x300>", thumb: "100x100>" },
    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :cover, content_type: /\Aimage\/.*\Z/
  validates_attachment_presence :cover
  validates :title, presence: true

end
