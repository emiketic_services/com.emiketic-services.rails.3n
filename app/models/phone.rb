# == Schema Information
#
# Table name: phones
#
#  id         :integer          not null, primary key
#  number     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  detail_id  :integer
#

class Phone < ActiveRecord::Base
  belongs_to :detail
end
