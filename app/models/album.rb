# == Schema Information
#
# Table name: albums
#
#  id         :integer          not null, primary key
#  name       :string
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Album < ActiveRecord::Base
  belongs_to :product
  has_many :images
  accepts_nested_attributes_for :images, allow_destroy: true

  validates_presence_of :images, :product
end
