# == Schema Information
#
# Table name: sliders
#
#  id                        :integer          not null, primary key
#  name                      :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  activated                 :boolean          default(TRUE), not null
#  description               :text
#  slider_image_file_name    :string
#  slider_image_content_type :string
#  slider_image_file_size    :integer
#  slider_image_updated_at   :datetime
#

class Slider < ActiveRecord::Base

  has_attached_file :slider_image, styles: { medium: "300x300>", thumb: "100x100>" },
    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :slider_image, content_type: /\Aimage\/.*\Z/
  validates_attachment_presence :slider_image
  scope :activated, -> { where(activated: true) }

end
