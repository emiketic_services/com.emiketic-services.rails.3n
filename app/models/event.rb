# == Schema Information
#
# Table name: events
#
#  id                        :integer          not null, primary key
#  name                      :string
#  description               :text
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  event_image_file_name     :string
#  event_image_content_type  :string
#  event_image_file_size     :integer
#  event_image_updated_at    :datetime
#  start_date                :datetime
#  end_date                  :datetime
#  document_pdf_file_name    :string
#  document_pdf_content_type :string
#  document_pdf_file_size    :integer
#  document_pdf_updated_at   :datetime
#

class Event < ActiveRecord::Base
  just_define_datetime_picker :start_date
  validates :start_date, :presence => true

  just_define_datetime_picker :end_date
  validates :end_date, :presence => true

  has_attached_file :event_image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :event_image, content_type: /\Aimage\/.*\Z/

end
