# == Schema Information
#
# Table name: products
#
#  id                        :integer          not null, primary key
#  name                      :string
#  description               :text
#  price                     :decimal(, )
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  document_pdf_file_name    :string
#  document_pdf_content_type :string
#  document_pdf_file_size    :integer
#  document_pdf_updated_at   :datetime
#  category_id               :integer
#  slug                      :string
#  promotion_id              :integer
#  url                       :string
#  new                       :boolean          default(FALSE), not null
#  status                    :string
#  state                     :string
#  user_id                   :integer
#  video_url                 :string
#

class Product < ActiveRecord::Base

  acts_as_commentable

  belongs_to :category
  belongs_to :promotion
  has_many :featured_products

  has_many :comments, as: :commentable

  has_many :images
  accepts_nested_attributes_for :images,
    allow_destroy: true,
    reject_if: lambda { |attributes| attributes[:content].blank? }

  extend FriendlyId
  friendly_id :name, use: :slugged

  validates :name, presence: true, length: { minimum: 5 }
  validates :description, :category, presence: true

  has_attached_file :document_pdf
  validates_attachment :document_pdf, :content_type => { :content_type => %w(application/pdf), message: "Vous devez inserer un document de format PDF" }

  PRODUCT_STATUS = ["En promotion", "En stock", "Sur commande", "Import en cours"]
  scope :is_new, -> { where(new: true) }
  scope :is_available, -> {where(state: "En stock")}
end
