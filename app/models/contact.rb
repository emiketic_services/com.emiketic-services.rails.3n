# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  email      :string
#  message    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  product_id :integer
#  firstname  :string
#  lastname   :string
#  phone      :string
#  fax        :string
#  company    :string
#  address    :string
#

class Contact < ActiveRecord::Base
  belongs_to :product
  validates :email, :firstname, :lastname, :phone, :message, presence:true
end
