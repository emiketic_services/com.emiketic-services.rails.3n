# == Schema Information
#
# Table name: categories
#
#  id                         :integer          not null, primary key
#  name                       :string
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  slug                       :string
#  parent_id                  :integer
#  lft                        :integer
#  rgt                        :integer
#  depth                      :integer
#  children_count             :integer
#  icon_file_name             :string
#  icon_content_type          :string
#  icon_file_size             :integer
#  icon_updated_at            :datetime
#  description                :text
#  icon_home_file_name        :string
#  icon_home_content_type     :string
#  icon_home_file_size        :integer
#  icon_home_updated_at       :datetime
#  rank                       :integer          default(0)
#  background_file_name       :string
#  background_content_type    :string
#  background_file_size       :integer
#  background_updated_at      :datetime
#  active                     :boolean          default(TRUE), not null
#  delete_icon                :boolean          default(FALSE), not null
#  delete_home_icon           :boolean          default(FALSE), not null
#  delete_background          :boolean          default(FALSE), not null
#  acceuil_photo_file_name    :string
#  acceuil_photo_content_type :string
#  acceuil_photo_file_size    :integer
#  acceuil_photo_updated_at   :datetime
#

class Category < ActiveRecord::Base
  acts_as_nested_set
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :products

  scope :categories, -> { except(:order).order('rank DESC') }
  scope :sub_category, -> {where.not(parent_id: nil).pluck(:name)}
  default_scope { order('name ASC') }

  has_attached_file :icon, :styles => { small: "64x64>", xxsmall: "84x84>", xsmall: "128x128>", :medium => "300x300>", :thumb => "100x100#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :icon, :content_type => /\Aimage\/.*\Z/

  has_attached_file :icon_home, :styles => { small: "32x32>",xsmall: "64x64>", :medium => "300x300>", :thumb => "100x100#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :icon_home, :content_type => /\Aimage\/.*\Z/

  has_attached_file :background, :styles => { small: "32x32>",xsmall: "64x64>", :medium => "300x300>", :large => "1300x400#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :background, :content_type => /\Aimage\/.*\Z/

  has_attached_file :acceuil_photo, :styles => {:medium => "300x300>", :large => "1300x400#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :acceuil_photo, :content_type => /\Aimage\/.*\Z/

  PARENT_CATEGORIES = ["Systèmes d'alarme","Systèmes de détection Incendie","Vidéo de surveillance","Vidéophonie/ Interphonie","Contrôle d'accès", "Contrôle des rondes","Portails coulissants","portes basculantes et barrières automatiques","Les miroirs","portiques","Détecteurs à main","Scanners à Bagages à Rayons X et Détecteurs de traces d'explosifs"]

  def related_products
    if self.root?
      categories = self.children
      root_categories = self.id
      Product.where(category_id: [categories.pluck(:id),self.id] )
    else
      self.products
    end
  end
  def should_generate_new_friendly_id?
    name_changed? || slug.blank?
  end

  attr_accessor :delete_icon, :delete_home_icon, :delete_background
  before_validation { self.icon.clear if self.delete_icon == '1' }
  before_validation { self.icon_home.clear if self.delete_home_icon == '1' }
  before_validation { self.background.clear if self.delete_background == '1' }
end
